#!/bin/bash


export PYTHON_PATH=/Library/Frameworks/Python.framework/Versions/2.7/bin/python

. DANKU_VERSION
rm ../cordova/www
ln -s ../danku-svelte-io/build/ ../cordova/www
source ../bin/cordova-build.sh
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
fi


