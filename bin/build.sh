#!/bin/bash

source ../bin/new-version.sh



rm -rf build .svelte-kit/output

npm run build 
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
fi
