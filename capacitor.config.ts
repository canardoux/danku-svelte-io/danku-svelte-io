import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'xyz.canardoux.danku',
  appName: 'Dank u',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
